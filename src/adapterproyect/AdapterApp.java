
package adapterproyect;

import motors.AdaptadorCombustionElectrico;
import moviles.Automovil;
import motors.MotorComun;
import motors.MotorEconomico;
import motors.MotorElectrico;

/**
 *
 * @author Dorme Ornimus
 */
public class AdapterApp {
    public static void main(String[] args) {
    Automovil auto = new Automovil(new MotorComun());
    System.out.println("Colocando motor comun--------------------------");
    auto.recorrido();
    System.out.println("----------------------------------------------------");
    System.out.println("Colocando motor economico--------------------------");
    auto.setMotor(new MotorEconomico());
    auto.recorrido();
    System.out.println("----------------------------------------------------");
    System.out.println("Colocando motor electrico--------------------------");
    auto.setMotor(new AdaptadorCombustionElectrico(new MotorElectrico()));
    auto.recorrido();
    System.out.println("----------------------------------------------------");
    }
}
