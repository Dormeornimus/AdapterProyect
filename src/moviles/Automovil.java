package moviles;


import motors.ControladorMotor;



/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:43:15 a. m.
 */
public class Automovil {

	private ControladorMotor motor;

	public Automovil(ControladorMotor motor){
            this.motor = motor;
	}

	public void acelerar(){
            this.motor.acelerar();
	}

	public void apagar(){
            this.motor.apagar();
	}

	public void encender(){
            this.motor.encender();
	}

	public void setMotor(ControladorMotor motor){
            this.motor = motor;
	}

	public String toString(){
            return null;
	}
        
        public void recorrido(){
            this.motor.recorrido();
        }

}