package motors;



/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:43:26 a. m.
 */
public interface ControladorMotorElectrico {

	public void activar();

	public void conectar();

	public void desconectar();

	public void detener();

	public void moverMasRapido();

}