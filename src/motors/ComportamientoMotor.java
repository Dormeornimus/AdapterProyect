/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package motors;

/**
 *
 * @author Dorme Ornimus
 */
public abstract class ComportamientoMotor implements ControladorMotor{
    
    @Override
    public abstract void acelerar();
    
    @Override
    public abstract void apagar();

    @Override
    public abstract void encender();
    
    @Override
    public void recorrido(){
        this.encender();
        this.acelerar();
        this.apagar();
    }
}
