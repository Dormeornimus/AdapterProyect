package motors;



/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:43:21 a. m.
 */
public interface ControladorMotor {

	public void acelerar();

	public void apagar();

	public void encender();
        
        public void recorrido();
}