package motors;


import motors.ControladorMotor;



/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:43:28 a. m.
 */
public class MotorEconomico extends ComportamientoMotor {

	public MotorEconomico(){

	}

	public void acelerar(){
            System.out.println("Acelerando motor economico");
	}

	public void apagar(){
            System.out.println("Apagando motor economico");
	}

	public void encender(){
            System.out.println("Encendiendo motor economico");
	}

}