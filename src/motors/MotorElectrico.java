package motors;


import motors.ControladorMotorElectrico;



/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:43:29 a. m.
 */
public class MotorElectrico implements ControladorMotorElectrico {

	public MotorElectrico(){

	}

	public void activar(){
            System.out.println("El motor electrico se ha activado");
	}

	public void conectar(){
            System.out.println("El motor electrico se ha conectado");
	}

	public void desconectar(){
            System.out.println("El motor electrico se ha desconectado");
	}

	public void detener(){
            System.out.println("El motor electrico se ha detenido");
	}

	public void moverMasRapido(){
            System.out.println("El motor electrico esta acelerando");
	}

}