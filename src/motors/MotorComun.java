package motors;


import motors.ControladorMotor;



/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:43:27 a. m.
 */
public class MotorComun extends ComportamientoMotor {

	public MotorComun(){

	}

	public void acelerar(){
            System.out.println("Acelerando motor comun");
	}

	public void apagar(){
            System.out.println("Apagando motor comun");
	}

	public void encender(){
            System.out.println("Encendiendo motor comun");
	}

}