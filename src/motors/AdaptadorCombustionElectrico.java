package motors;

    

/**
 * @author Dorme Ornimus
 * @version 1.0
 * @created 18-may.-2018 09:42:59 a. m.
 */
public class AdaptadorCombustionElectrico extends ComportamientoMotor {

	private MotorElectrico motorelectrico;

	public AdaptadorCombustionElectrico(MotorElectrico motor){
            this.motorelectrico = motor;
	}

	public void acelerar(){
            motorelectrico.moverMasRapido();
	}

	public void apagar(){
            motorelectrico.detener();
            motorelectrico.desconectar();
	}

	public void encender(){
            motorelectrico.conectar();
            motorelectrico.activar();
	}

}